const mongoose = require('mongoose')
// remover alerta de warning
mongoose.Promise = global.Promise

module.exports = mongoose.connect('mongodb://localhost/todo')
